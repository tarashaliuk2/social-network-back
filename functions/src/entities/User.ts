import IUser from "../models/IUser";
import {Timestamp, WriteResult} from "../models/FirebaseTypes";
import * as admin from "firebase-admin";
import UserRepository from "../repositories/UserRepository";

export default class User implements IUser {
    id!: string;
    createdAt!: Timestamp | Date;
    email!: string;
    imageUrl!: string;

    // set createdAt as null to not update this field
    constructor(email: string, id?: string, imageUrl: string | null = null, createdAt?: Timestamp | Date | null) {
        this.email = email;
        if (createdAt !== null) {
            this.createdAt = createdAt ? createdAt : admin.firestore.Timestamp.fromDate(new Date());
        }
        if (imageUrl !== null) {
            this.imageUrl = imageUrl;
        }
        if (id) {
            this.id = id;
        }
    }

    public save(): Promise<WriteResult> {
        return UserRepository.create(this.getUser())
    }

    public update(): Promise<WriteResult>{
        return UserRepository.update(this.getUser())
    }

    public getUser(): IUser {
        const base: IUser = {
            email: this.email
        };
        if (this.createdAt) {
            base.createdAt = this.createdAt
        }
        if (this.imageUrl) {
            base.imageUrl = this.imageUrl
        }
        return this.id ? {...base, id: this.id} : base;
    }
}
