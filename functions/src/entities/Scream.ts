import IScream from "../models/IScream";
import {DocumentReference, Timestamp} from "../models/FirebaseTypes";
import * as admin from "firebase-admin";
import ScreamRepository from "../repositories/ScreamRepository";

export default class Scream implements IScream{
    id!: string;
    body!: string;
    createdAt!: Timestamp;
    screamHandle!: string;

    constructor(screamHandle: string, body: string, id?: string, createdAt?: Timestamp) {
        this.screamHandle = screamHandle;
        this.body = body;
        this.createdAt = createdAt ? createdAt : admin.firestore.Timestamp.fromDate(new Date());
        if (id) {
            this.id = id;
        }
    }

    public save(): Promise<DocumentReference> {
        return ScreamRepository.save(this.getScream())
    }

    public update() {
        // return ScreamRepository.update(this.id, this.getScream())
    }

    public getScream(): IScream {
        const base: IScream = {
            screamHandle: this.screamHandle,
            body: this.body,
            createdAt: this.createdAt
        };
        return this.id ? {...base, id: this.id} : base;
    }
}
