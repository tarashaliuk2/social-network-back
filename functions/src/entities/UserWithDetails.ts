import User from "./User";
import IUserWithDetails from "../models/IUserWithDetails";

export default class UserWithDetails extends User implements IUserWithDetails{
    bio!: string;
    website!: string;
    location!: number[];

    constructor(email: string) {
        super(email);
    }
}
