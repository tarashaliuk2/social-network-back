import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import * as express from 'express'
import * as firebase from 'firebase'
import AuthRoutes from './routes/AuthRoutes';
import ScreamRoutes from './routes/ScreamRoutes'
import UserRoutes from './routes/UserRoutes'
import firebaseConfig from './config'

const app = express();
admin.initializeApp();
firebase.initializeApp(firebaseConfig);

app.use(ScreamRoutes);
app.use(AuthRoutes);
app.use(UserRoutes);

exports.api = functions.region('europe-west1').https.onRequest(app);
