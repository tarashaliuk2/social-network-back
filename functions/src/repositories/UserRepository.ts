import * as db from "firebase-admin";
import {QuerySnapshot, WriteResult} from "../models/FirebaseTypes";
import IUser from "../models/IUser";

export default class UserRepository {
    private static readonly collection: string = 'users';

    static async getUser(id: string) {
        return db.firestore()
            .collection(this.collection)
            .doc(id)
            .get();
    };

    static async getUserByField(field: string, value: any): Promise<QuerySnapshot> {
        return db.firestore()
            .collection(this.collection)
            .where(field, '==', value)
            .get();
    }

    static async create(data: IUser): Promise<WriteResult> {
        const id: string = data.id!;
        delete data.id;
        return db.firestore()
            .collection(this.collection)
            .doc(id)
            .set(data);
    }

    static async update(data: IUser): Promise<WriteResult> {
        const id: string = data.id!;
        delete data.id;
        return db.firestore()
            .collection(this.collection)
            .doc(id)
            .update(data);
    }
}

