import * as admin from "firebase-admin";
import IFile from "../models/IFile";

export default class StorageRepository {
    static async upload(file: IFile, fileName: string, path?: string) {
        console.log('file', file);
        console.log('file.mimetype', file.mimetype);
        const destination = path ? `${path}/${fileName}` : fileName;
        return admin
            .storage()
            .bucket()
            .file(`${destination}.${file.originalname.split('.').pop()}`).save(file.buffer, {
                contentType: file.mimetype
            })
        // const bucket = admin.storage().bucket(config.storageBucket);
        // return bucket.file(`usersAvatars/${file.originalname}`).save(file.buffer, {
        //     contentType: file.mimetype,
        //     resumable: false
        // })
    }
}
