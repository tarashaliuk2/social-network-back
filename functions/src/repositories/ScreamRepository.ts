import * as admin from "firebase-admin";
import IScream from "../models/IScream";
import {DocumentReference} from "../models/FirebaseTypes";

export default class ScreamRepository {
    private static collection: string = 'screams';

    static async getAll(orderBy?: string, desc?: boolean) {
        const query = admin.firestore()
            .collection(this.collection);
        if (orderBy) {
            query.orderBy(orderBy, desc ? 'desc' : 'asc')
        }
        return query.get();
    }

    static async save(data: IScream): Promise<DocumentReference> {
        return admin.firestore()
            .collection(this.collection)
            .add(data);
    }
}
