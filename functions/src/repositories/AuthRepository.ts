import {auth} from "firebase";

export default class AuthRepository {
    static async signup(email: string, password: string): Promise<auth.UserCredential> {
        return auth().createUserWithEmailAndPassword(email, password)
    }

    static async authorize(email: string, password: string): Promise<auth.UserCredential> {
        return auth().signInWithEmailAndPassword(email, password)
    }
}
