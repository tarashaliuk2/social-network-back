import IScream from "../models/IScream";
import {DocumentData} from "../models/FirebaseTypes";
import * as admin from "firebase-admin";

export default class ScreamFactory {
    static toScream(data: DocumentData, id?: string|null): IScream {
        const base: IScream = {
            screamHandle: data.screamHandle,
            body: data.body,
            createdAt: data.createdAt.toDate
        };
        return id ? {...base, id} : base
    }

    static toNewScream(data: any): IScream {
        return {
            screamHandle: data.screamHandle,
            body: data.body,
            createdAt: admin.firestore.Timestamp.fromDate(new Date())
        }
    }
}

