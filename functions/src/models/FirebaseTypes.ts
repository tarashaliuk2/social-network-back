import * as firebase from "firebase-admin";

export type QuerySnapshot = firebase.firestore.QuerySnapshot;
export type DocumentReference = firebase.firestore.DocumentReference;
export type WriteResult = firebase.firestore.WriteResult;

export type DocumentData = firebase.firestore.DocumentData;
export type GeoPoint = firebase.firestore.GeoPoint;
export type Timestamp = firebase.firestore.Timestamp
export type CollectionReference = firebase.firestore.CollectionReference;
export type Transaction = firebase.firestore.Transaction;
export type QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;
