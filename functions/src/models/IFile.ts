export default interface IFile {
    fieldname: string,
    originalname: string,
    encoding: string,
    filepath: string,
    mimetype: string,
    buffer: Buffer,
    size: number
}
