import {Timestamp} from "./FirebaseTypes";

export default interface IScream {
    id?: string,
    screamHandle: string,
    body: string,
    createdAt: Timestamp | Date
}
