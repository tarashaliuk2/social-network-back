import {Timestamp} from "./FirebaseTypes";

export default interface IUser {
    id?: string,
    email: string,
    createdAt?: Timestamp | Date,
    imageUrl?: string
}
