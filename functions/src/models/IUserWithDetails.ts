import IUser from "./IUser";

export default interface IUserWithDetails extends IUser {
    bio?: string,
    website?: string,
    location?: number[]
}
