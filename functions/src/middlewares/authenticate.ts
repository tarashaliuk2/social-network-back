import {Response, NextFunction} from "express";
import * as admin from "firebase-admin";
import {auth} from 'firebase'

export const authMiddleware = async (req: any, res: Response, next: NextFunction) => {
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        const token = req.headers.authorization.split('Bearer ')[1];
        try {
            const decodedToken = await admin.auth().verifyIdToken(token);
            await auth().currentUser?.getIdToken(true);
            req.user = decodedToken;
            next();
            return
        } catch (e) {
            return res.status(401).json(e.message)
        }
    } else {
        return res.status(401).json({error: 'Unauthorized'})
    }
};
