import {body} from 'express-validator'

export const createScreamValidationRules = [
    body('screamHandle').isString(),
    body('body').isString()
];

