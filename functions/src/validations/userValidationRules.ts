import {body} from 'express-validator'

export const userDetailsValidationRules = [
    body('bio').isString(),
    body('website').isString(),
    body('location').isArray().custom((value) =>
        value.length === 2 && !isNaN(value[0]) && !isNaN(value[1]) && value[0] > 0 && value[1] > 0)
];
