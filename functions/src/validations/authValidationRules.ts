import {body} from 'express-validator'

export const signUpValidationRules = [
    body('email').isEmail(),
    body('password').isLength({min: 6}),
    body('confirmPassword', 'Passwords don\'t match').custom((value, {req}) => value === req.body.password),
];

export const signInValidationRules = [
    body('email').isEmail(),
    body('password').isLength({min: 6})
];

