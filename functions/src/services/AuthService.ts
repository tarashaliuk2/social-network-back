import AuthRepository from "../repositories/AuthRepository";
import {User, auth} from "firebase";

export default class AuthService {
    static async register(email: string, password: string): Promise<User> {
        const registerResult: auth.UserCredential = await AuthRepository.signup(email, password);
        if (registerResult.user) {
            return registerResult.user;
        }
        throw new Error('Can not register user')
    }

    static async authorize(email: string, password: string): Promise<string> {
        const loginResult = await AuthRepository.authorize(email, password)
        if(loginResult.user) {
            return loginResult.user.getIdToken()
        }
        throw new Error('Invalid credentials')
    }
}
