import UserRepository from "../repositories/UserRepository";
import StorageRepository from "../repositories/StorageRepository";
import IFile from "../models/IFile";

export default class UserService {
    static async userExists(email: string): Promise<boolean> {
        const users = await UserRepository.getUserByField('email', email);
        return !!users.docs.map(item => item.data()).length;
    }

    static async uploadUserImage(file: IFile, userId: string) {

        return StorageRepository.upload(file, userId, 'usersAvatars')
        // const BusBoy = require('busboy');
        // const path = require('path');
        // const os = require('os');
        // const fs = require('fs');
        //
        // const busboy = new BusBoy({
        //     headers
        // });
        //
        // const imageToBeUpload = {
        //     filepath: '',
        //     mimetype: ''
        // };
        // let imageFileName = '';
        //
        // busboy.on('file', (fieldname: string, file: any, filename: string, encoding: string, mimetype: string) => {
        //     const fileExtension: string = filename.split('.')[filename.split('.').length - 1];
        //     // @ts-ignore
        //     imageFileName = `${userId}.${fileExtension}`;
        //     const filepath = path.join(os.tmpdir(), imageFileName);
        //     imageToBeUpload.filepath = filepath;
        //     imageToBeUpload.mimetype = mimetype;
        //     file.pipe(fs.createWriteStream(filepath))
        // });
        //
        // busboy.on('finish', async () => {
        //     await admin.storage().bucket().upload(imageToBeUpload.filepath, {
        //         resumable: false,
        //         metadata: {
        //             metadata: {
        //                 contentType: imageToBeUpload.mimetype
        //             }
        //         }
        //     });
        //     const imageUrl = `http://firebase.googleapis.com/v0/b/${config.storageBucket}/o/${imageFileName}?alt=media`;
        //     // @ts-ignore
        //     const user = new User(userEmail, userId, imageUrl);
        //     await user.update()
        // });
        // busboy.end(rawBody)
    }
}
