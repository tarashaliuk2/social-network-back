import {validationResult} from "express-validator"
import {NextFunction, Request, Response} from "express";

export default (req: Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        next();
        return;
    }
    const extractedErrors: object[] = [];
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))

    return res.status(422).json({
        errors: extractedErrors,
    })
}
