import {Request, Response} from "express";
import UserService from "../services/UserService";
import AuthService from "../services/AuthService";
import User from "../entities/User";

export default class AuthController {
    static async signUp(req: Request, res: Response) {
        try {
            if (await UserService.userExists(req.body.email)) {
                throw new Error('Email is already taken');
            }

            const registerResult = await AuthService.register(req.body.email, req.body.password);

            const user: User = new User(req.body.email, registerResult.uid);
            await user.save();

            res.status(201).json({
                message: `user ${registerResult.uid} sign up successfully`,
            })
        } catch (e) {
            console.error(e);
            res.status(401).send(e.message)
        }
    }

    static async signIn(req: Request, res: Response) {
        try {
            const token: string = await AuthService.authorize(req.body.email, req.body.password);

            res.json({token})
        } catch (e) {
            res.status(403).send(e.message)
        }
    }
}
