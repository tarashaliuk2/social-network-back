import {Request, Response} from "express";
import ScreamRepository from "../repositories/ScreamRepository";
import IScream from "../models/IScream";
import ScreamFactory from "../factories/ScreamFactory";
import Scream from "../entities/Scream";

export default class ScreamController {
    static async index(req: Request, res: Response) {
        try {
            const response = await ScreamRepository.getAll('createdAt');
            const screams: IScream[] = [];
            response.forEach(doc => {
                // screams.push(ScreamFactory.toScream(doc.data(), doc.id))
                screams.push(ScreamFactory.toScream(doc.data(), doc.id))
            });
            res.json(screams);
        } catch (e) {
            console.error(e);
            res.status(401).send(e)
        }
    }

    static async create(req: Request, res: Response) {
        try {
            const newScream: Scream = new Scream(req.body.screamHandle, req.body.body);
            console.log('newScream', newScream.getScream());
            const doc = await newScream.save();
            res.json({newScreamId: doc.id})
        } catch (e) {
            console.error(e);
            res.status(422).send(e)
        }
    }
}
