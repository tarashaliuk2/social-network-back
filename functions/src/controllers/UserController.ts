import {Response} from "express";
import UserService from "../services/UserService";
import User from "../entities/User";
import config from "../config";

export default class UserController {
    static async uploadUserImage(req: any, res: Response) {
        try {
            await UserService.uploadUserImage(req.files[0], req.user.user_id);
            const user = new User(
                req.user.email,
                req.user.user_id,
                `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/usersAvatars%2F${req.user.user_id}.${req.files[0].originalname.split('.').pop()}?alt=media`,
                null
            );
            await user.update();
        } catch (e) {
            console.warn(e);
            res.status(422).json({
                message: e.message
            })
        }
        res.json({message: 'Image uploaded'})
    }

    static async addUserDetails(req: any, res: Response) {
        // const userDetails = reduceUserDetails()
    }
}
