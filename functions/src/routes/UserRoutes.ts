import {Router} from 'express'
import {authMiddleware} from "../middlewares/authenticate";
import UserController from "../controllers/UserController";
import fileHandlerMiddleware from "../middlewares/fileHandlerMiddleware";
import validate from "../utils/validate";
import {userDetailsValidationRules} from "../validations/userValidationRules";

const router = Router();

router.post('/user/image',
    authMiddleware, fileHandlerMiddleware,
    UserController.uploadUserImage
);
router.post('/user',
    authMiddleware,
    userDetailsValidationRules, validate,
    UserController.addUserDetails
);

export default router
