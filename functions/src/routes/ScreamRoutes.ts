import {Router} from 'express'
import {createScreamValidationRules} from "../validations/screamValidationRules";
import validate from "../utils/validate";
import {authMiddleware} from "../middlewares/authenticate";
import ScreamController from "../controllers/ScreamController";

const router = Router();

router.get('/scream',
    authMiddleware,
    ScreamController.index
);

router.post('/scream',
    authMiddleware,
    createScreamValidationRules,
    validate,
    ScreamController.create
);

export default router
