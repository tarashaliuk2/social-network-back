import {Router} from "express";
import AuthController from "../controllers/AuthController";
import {signInValidationRules, signUpValidationRules} from "../validations/authValidationRules";
import validate from "../utils/validate";

const router = Router();

router.post('/signup',
    signUpValidationRules,
    validate,
    AuthController.signUp
);

router.post('/signin',
    signInValidationRules,
    validate,
    AuthController.signIn

);

export default router
